'use strict';

module.exports = function(Estadio) {
    Estadio.disableRemoteMethodByName('deleteById');
};
