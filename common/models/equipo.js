'use strict';

module.exports = function(Equipo) {
    Equipo.observe('before save', (context, next) => {
        if (context.instance) {
          context.instance.updated = new Date();
        }
        next();
      })
Equipo.jugadores=function(idEquipo, idJugador,cb){

    Equipo.findById(idEquipo,(err,equipo)=>{

        if(equipo,jugadores.some(jugador=>jugador == idJugador)){
            cd('jugador existe', null)
        }else{
            equipo.jugadores.push(idJugador);
        equipo.updateAttribute('jugadores',equipo.jugadores,(err,equipo)=>{
            cb(null,equipo)
        })
        }


        
    })

    cb('operacion no procesada', null)

}
Equipo.remoteMethod('jugadores',{
    description:'agregar jugadores al equipo',
    accepts:[{
        arg:'idEquipo',
        type:"string"
    },{
        arg:'idJugador',
        type:'string'
    }],
    returns:{
        arg:'equipo',
        type:'object'
    }
})

Equipo.listarJugadores=function(idEquipo,cb){
    Equipo.findById(idEquipo,
        (err,equipo)=>{
            Equipo.app.models.jugador.find({
                where:{
                    id:{
                        inq:equipo.jugadores
                    }
                }
            },(err,jugadores)=>{
                cb(null,jugadores)
            })
        })
}

Equipo.remoteMethod('listarJugadores', {
    description: 'Mostrar jugadores del equipo',
    http: {
      path: '/:idEquipo/jugadores',
      verb: 'get'
    },
    accepts: {
      arg: 'idEquipo',
      type: 'string'
    },
    returns: {
      arg: 'jugadores',
      type: 'object'
    }
  })
};
